﻿using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;
using Bet365;
using System.Threading;
using System;
using NATS.Client;
using System.Text;
using Newtonsoft.Json;
using Modelo.NATS;
using System.IO;
using Modelo.Bet365;
using System.Threading.Tasks;

namespace Bet365Bot
{
    public class Bot : DataFirst.Bot
    {
        //INICIO CODIGO NUEVA ESTRUCTURA
        public void ListarEncuentros()
        {
            List<IWebElement> el = new List<IWebElement>();

            el.AddRange(
                WebDriver.
                    FindElements(
                        By.
                            XPath("/html/body/div[1]/div/div[2]/div[1]/div/div/div[2]/div[1]/div/div/div/div[3]/div[1]")
                        )
            );

            List<string> ag = new List<string>();

            for (int b = 0; b < el.Count; b++)
            {
                ag.Add(el[b].GetAttribute("outerHTML"));
            }

            string v1 = "";
        }
        //FIN CODIGO NUEVA ESTRUCTURA

        private string USER ;
        private string PASS;

        EventHandler<MsgHandlerEventArgs> eventHandler;
        internal bool enIframe { get; set; }
        internal List<Apuesta> apuestasPendientes;
        public decimal CuotaMinima { get; set; }
        public bool Apostando { get; set; }
        public void logBet(string log, bool Append = true)//Si append es false pisará el archivo
        {
            try
            {
                StreamWriter sw = new StreamWriter("C:\\Logs\\Bet365App_logBet.log", Append);
                sw.WriteLine(DateTime.Now.ToString() + ": " + log);
                sw.Dispose();
                sw.Close();
                Console.WriteLine(log);
                Console.ReadLine();
            }
            catch
            {
                Console.WriteLine(log);
                Console.ReadLine();
            }
        }

        public Bot()
        {
            enIframe = false;
            Apostando = false;     
        }

        NatsHandler natsHandler = new NatsHandler("nats://ec2-3-124-242-27.eu-central-1.compute.amazonaws.com");
        Task taskPrincipal;
        public void Main(string user, string pass)
        {
            logBet("Inicializando entorno",false);

            USER = user;
            PASS = pass;
            taskPrincipal = new Task(() => LoopPrincipal(user, pass));
            apuestasPendientes = new List<Apuesta>();
            IniciarSesion(user, pass, SEGURO: true);
            eventHandler = (sender, args) =>
            //evento que dispara nat cuando llega un mensaje
            {
                try
                {
                    string data = Encoding.UTF8.GetString(args.Message.Data, 0, args.Message.Data.Length);
                    Señal señal = JsonConvert.DeserializeObject<Señal>(data);
                    logBet("Señal recibida /"+ señal.EquipoLocal + "v" + señal.EquipoVisitante +"/" + señal.Estrategia);

                    Apuesta apuestaInicial = new Apuesta();
                    apuestaInicial.Local = señal.EquipoLocal;
                    apuestaInicial.Visitante = señal.EquipoVisitante;
                    apuestaInicial.Estrategia = señal.Estrategia.ToUpper();
                    apuestaInicial.Monto = Convert.ToDecimal(0.2);
                    apuestaInicial.CuotaMinima = Convert.ToDouble(CuotaMinima);
                    apuestaInicial.CornersLocalPrimeraLectura = int.Parse(señal.CornersLocal);
                    apuestaInicial.CornersVisitantelPrimeraLectura = int.Parse(señal.CornersVisitante);
                    apuestaInicial.CornersLocal = apuestaInicial.CornersLocalPrimeraLectura;
                    apuestaInicial.CornersVisitante = apuestaInicial.CornersVisitantelPrimeraLectura;
                    apuestaInicial.horaInicioEjecucion = DateTime.Now.AddSeconds(-3);

                    apuestasPendientes.Add(apuestaInicial);

                }
                catch (Exception ex)
                {
                    logBet("Error manejando el evento de la señal: " + ex.Message.ToString());
                }
            };
            natsHandler.Suscribe(eventHandler);
            taskPrincipal.Start();
            //Task.Run(() => LoopPrincipal(user, pass));

        }

        public void LoopPrincipal(string user,string pass)
        {
            while (true)
            {
                try
                {
                    IniciarSesion(user, pass);
                    //RecorrerPartidos();
                    //Si no está apostando, apostar, sino esperar turno                
                    if (apuestasPendientes.Count > 0)
                    {
                        Apuesta apuestaSeleccionada = apuestasPendientes[0]; //Siempre va a tomar la primera

                        if (apuestaSeleccionada != null)
                        {//Si hay una apuesta con hora de inicio inferior a la actual

                            logBet("Inicio de apuesta");
                            Apostando = true;

                            Apostar(apuestaSeleccionada);
                        }
                        else
                        {
                            logBet("Apuestas encoladas: " + apuestasPendientes.Count.ToString());
                        }
                    }

                    Thread.Sleep(1000);
                }
                catch(Exception ex)
                {
                    logBet("LoopPrincipal " + ex.ToString());
                }
                           
            }
            logBet("==========================================================");
            logBet("LOOP CERRADO");

        }

        public void insertarApuesta(Apuesta apuesta)
        {
            using (MagicSQL.Tn tn = new MagicSQL.Tn("Bet365"))
            {
                try
                {
                    Cuenta cuenta = new Cuenta().Select(tn).Where(it => it.Usuario == USER).FirstOrDefault();
                    if (cuenta != null)
                    {
                        apuesta.IdCuenta = cuenta.IdCuenta;
                        apuesta.Insert(tn);
                        tn.Commit();
                    }
                }
                catch (Exception ex)
                {
                    logBet("Registro de apuesta en base de datos ha fallado. " + ex.Message);
                    tn.RollBack();
                }
                finally
                {
                    tn.Dispose();
                }
            }
        }
        

        public void IniciarSesion(string user, string pass, bool SEGURO = false)
        {
            try
            {
                if (!WebDriver.Url.Contains("www.bet365"))
                    IrA("https://www.bet365.com/#/IP/");
                EsperarPor(".//div[@class='alm-InactivityAlert_Remain ' and text()='Permanecer Conectado']", JSCLICK: true, INTERVALO: 0);
            }
            catch { }
            

            string XPATH_campo_usuario_login_principal = ".//div[@class='hm-Login_UserNameWrapper ']/input[@class='hm-Login_InputField ']";
            string XPATH_campo_pass_login_principal = ".//div[@class='hm-Login_PasswordWrapper ']/input[@class='hm-Login_InputField ']";
            string usuarioBet365 = user;
            string passBet365 = pass;
            while (true)
            {
                if (SEGURO) //si es un login seguro el esperar por se toma su tiempo para q aparezca el objeto
                {
                    EsperarPor(XPATH_campo_usuario_login_principal);
                }
                if (EsperarPor(XPATH_campo_usuario_login_principal, INTERVALO: 0))//chequear si existe el campo login rapido
                {
                    try
                    {
                        WebDriver.FindElementByXPath(XPATH_campo_usuario_login_principal).Click();
                        Thread.Sleep(500);
                        WebDriver.FindElementByXPath(XPATH_campo_usuario_login_principal).SendKeys(usuarioBet365);
                        Thread.Sleep(700);
                        WebDriver.FindElementByXPath(XPATH_campo_pass_login_principal).Click();
                        Thread.Sleep(300);
                        WebDriver.FindElementByXPath(XPATH_campo_pass_login_principal).SendKeys(passBet365);
                        Thread.Sleep(700);
                        WebDriver.FindElementByXPath(".//button[@class='hm-Login_LoginBtn ']").Click();
                        Thread.Sleep(500);
                        //WebDriver.FindElementByXPath(".//div[@class='ip-ControlBar_BBarItem ' and text()='Evento']").Click();
                        Thread.Sleep(1000);
                        EsperarPor(".//div[@class='hm-UserName_UserNameShown ']");
                        logBet("Sesión de " + user + " iniciada");
                        WebDriver.SwitchTo().DefaultContent();
                        Thread.Sleep(1000);
                    }
                    catch
                    {
                        logBet("reintentando el inicio de sesion...");
                        continue;
                    }
                }

                try
                {
                    EsperarPor(".//div[@class='hm-UserName_UserNameShown ']");
                    if (!WebDriver.FindElementByXPath(".//div[@class='hm-UserName_UserNameShown ']").Text.Equals(usuarioBet365))
                    {
                        logBet("cerrando sesion usuario incorrecto");

                        WebDriver.FindElementByXPath(".//div[@class='hm-MembersInfoButton_AccountIcon ']").Click();
                        Thread.Sleep(1000);
                        WebDriver.FindElementByXPath(".//div[contains(@class,'LoggedInLogOut')]").Click();
                        Thread.Sleep(1000);
                    }
                    else
                    {
                        if (SEGURO)
                        {
                            EsperarPor(".//div[@class='pm-PushTargetedMessageOverlay_CloseButton ' and text()='Cerrar']",JSCLICK:true,REINTENTOS:5,INTERVALO:1);
                            SEGURO = false;
                        }
                        //usuario logueado igual que el deSeado
                        if (!EsperarPor(".//div[@class='hm-HeaderModule ']//a[text()='Directo' and @class='hm-BigButton hm-BigButton_Highlight ']", INTERVALO: 0))
                        {
                            EsperarPor(".//div[@class='hm-HeaderModule ']//a[text()='Directo' and @class='hm-BigButton ']", CLICKEAR: true);
                        }

                        EsperarPor(".//div[@class='ip-ControlBar ']");
                        if (!EsperarPor(".//div[@class='ip-ControlBar ']//div[contains(@class,'Selected') and text()='Evento']", INTERVALO: 0))
                        {
                            EsperarPor(".//div[@class='ip-ControlBar ']//div[@class='ip-ControlBar_BBarItem ' and text()='Evento']", CLICKEAR: true, INTERVALO: 0);
                        }
                        //

                        break;
                    }
                }
                catch {}
                //EsperarPor(".//div[@class='pm-PushTargetedMessageOverlay_CloseButton ' and text()='Cerrar']", CLICKEAR: true, REINTENTOS: 3, INTERVALO: 0);
            }
        }

        internal void RecorrerPartidos()
        {
            if (EsperarPor(".//span[@class='ipn-ClassificationButton_Label ' and text()='Fútbol']", REINTENTOS: 3, INTERVALO: 1))
            {
                //var titulo_futbol = this.WebDriver.FindElementByXPath(".//span[@class='ipn-ClassificationButton_Label ' and text()='Fútbol']");
                //var contendor_ligas = titulo_futbol.FindElement(By.XPath("../../div[@class='ipn-ClassificationContainer ']"));
                //while (true)
                //{
                //    try
                //    {
                //        foreach (var liga in contendor_ligas.FindElements(By.XPath(".//div[contains(@class,'ipn-Competition ')]")))
                //        {
                //            try
                //            {
                //                var clase = liga.GetAttribute("class").ToString();
                //                if (clase.Contains("closed"))
                //                {
                //                    liga.Click();
                //                }
                //            }
                //            catch
                //            {
                //                continue;
                //            }
                //        }
                //    }
                //    catch (Exception ex)
                //    {
                //        logBet("Sin partidos para recorrer");
                //    }
                //    break;
                //}

                //var ligas = contendor_ligas.FindElements(By.XPath(".//div[contains(@class,'ipn-CompetitionContainer ')]"));
                var ligas_futbol_xpath = ".//span[@class='ipn-ClassificationButton_Label ' and text()='Fútbol']../../div[@class='ipn-ClassificationContainer '].//div[contains(@class,'ipn-CompetitionContainer ')]";
                var ligas = WebDriver.FindElementsByXPath(ligas_futbol_xpath);
                List<Object> partidos = new List<Object>();
                foreach (var liga in ligas)
                {
                    while (true)
                    {
                        try
                        {
                            var _partidos = liga.FindElements(By.XPath(".//div[@class='ipn-TeamStack ']"));
                            foreach (var partido in _partidos)
                            {
                                var contenedor_partido = partido.FindElement(By.XPath("../../../div"));
                                //esto se ejecuta para todos los partidos (html object)
                                //escrapeo los primeros datos de la barra lateral
                                var equipos = contenedor_partido.FindElement(By.XPath(".//div")).Text.Split();
                                try
                                {
                                    equipos = contenedor_partido.FindElement(By.XPath(".//div[@class='ipn-TeamStack ']")).Text.Replace("\r", "").Split('\n');

                                }
                                catch
                                {
                                    continue;
                                }

                                var local = equipos[0];
                                var visitante = equipos[1];
                                var goles = new[] { "-1", "-1" };

                                try
                                {
                                    goles = partido.FindElement(By.XPath("..//div[@class='ipn-ScoreDisplayStandard_Score']")).Text.Split('-');
                                }
                                catch
                                {

                                    try
                                    {
                                        goles = partido.FindElement(By.XPath("..//div[@class='ipn-ScoreDisplayStandard_ScoreMediaAvailable")).Text.Split('-');
                                    }
                                    catch
                                    {
                                        continue;
                                    }
                                }
                                var goles_local = goles[0];
                                var goles_visitante = goles[1];
                                var reloj = partido.FindElement(By.XPath("..//div[@class='ipn-ScoreDisplayStandard_Timer']")).Text;

                                var EquipoGanando = "";
                                var EquipoPerdiendo = "";
                                if (int.Parse(goles_local) > int.Parse(goles_visitante) && int.Parse(goles_local) > -1)
                                {
                                    EquipoGanando = local;
                                    EquipoPerdiendo = visitante;
                                }
                                else if (int.Parse(goles_local) < int.Parse(goles_visitante) && int.Parse(goles_local) > -1)
                                {
                                    EquipoGanando = visitante;
                                    EquipoPerdiendo = local;
                                }
                                else if (int.Parse(goles_local) > -1)
                                {
                                    //empate
                                }

                                if (!this.EsperarPor(".//div[@class='ipe-GridHeader_FixtureCell ']", CLICKEAR: true,INTERVALO:0))
                                {
                                    continue; //saltear partido porque no carga
                                }
                            }
                        }
                        catch
                        {
                            logBet("No hay partidos");
                        }
                        break;
                    }
                }

            }
        }

        bool BuscarEncuentro(Apuesta apuesta)
        {
            bool result = false;

            logBet("Buscando encuentro " + apuesta.Local + " v " + apuesta.Visitante);
            var apuestaLocal = EstandarizarString(apuesta.Local);
            var apuestaVisitante = EstandarizarString(apuesta.Visitante);
            List<string> todos_los_equipos_string = new List<string>();
            if (!EsperarPor(".//span[@class='ipn-ClassificationButton_Label ' and text()='Fútbol']",INTERVALO:1,REINTENTOS:3))
            {
                apuesta.Basura = true;
                logBet("No hay partidos para leer");
            }
            else
            {
                var titulo_futbol = this.WebDriver.FindElementByXPath(".//span[@class='ipn-ClassificationButton_Label ' and text()='Fútbol']");
                var contendor_ligas = titulo_futbol.FindElement(By.XPath("../../div[@class='ipn-ClassificationContainer ']"));
                List<IWebElement> todos_los_equipos = new List<IWebElement>();
                while (true)
                {
                    try
                    {
                        todos_los_equipos = contendor_ligas.FindElements(By.XPath(".//div[@class='ipn-TeamStack_Team']")).ToList();

                        foreach (IWebElement _equipo in todos_los_equipos)
                        {
                            todos_los_equipos_string.Add(_equipo.Text.ToLower());
                        }
                        //logBet("Equipos listados: "+todos_los_equipos.Count.ToString());
                        break;
                    }
                    catch
                    {
                        logBet("Error recorriendo partidos, reintentando...");
                    }
                }



                foreach (IWebElement _partido in WebDriver.FindElementsByXPath(".//div[@class='ipn-ScoreDisplayBaseLabel_Name']").ToList())
                {
                    var equipos = _partido.Text.Split(new string[] { " v " }, StringSplitOptions.None);
                    try
                    {
                        string tmp_local = equipos[0].ToLower();
                        string tmp_visitante = equipos[1].ToLower();
                        todos_los_equipos_string.Add(tmp_local);
                        todos_los_equipos_string.Add(tmp_visitante);
                    }
                    catch { }
                }

                List<Equipo> listaEquiposLocal = new List<Equipo>();
                List<Equipo> listaEquiposVisitante = new List<Equipo>();

                bool local = true;
                int idPartido = 1;
                foreach (string equipoRecorrer in todos_los_equipos_string)
                {
                    Equipo equipoNuevo = new Equipo();
                    equipoNuevo.Id = idPartido;
                    equipoNuevo.Nombre = equipoRecorrer;

                    if (local == true)
                    {
                        listaEquiposLocal.Add(equipoNuevo);
                        local = false;
                    }
                    else
                    {
                        listaEquiposVisitante.Add(equipoNuevo);
                        idPartido++;
                        local = true;
                    }
                }

                if (listaEquiposLocal.Count > 0 && listaEquiposLocal.Count == listaEquiposVisitante.Count)
                {
                    Equipo equipoLocalMax = new Equipo();
                    Equipo equipoVisitanteMax = new Equipo();

                    equipoLocalMax.PuntosAproximacion = 0;
                    equipoLocalMax.CantidadAproximaciones = 0;

                    equipoVisitanteMax.PuntosAproximacion = 0;
                    equipoVisitanteMax.CantidadAproximaciones = 0;

                    foreach (Equipo equipoRecorrer in listaEquiposLocal)
                    {
                        int idPartidoRecorrer = (equipoRecorrer.Id - 1);

                        List<string> palabrasApuestaLocal = apuestaLocal.Split(' ').ToList();
                        List<string> palabrasEquipoLocal = listaEquiposLocal[idPartidoRecorrer].Nombre.Split(' ').ToList();

                        List<string> palabrasApuestaVisitante = apuestaVisitante.Split(' ').ToList();
                        List<string> palabrasEquipoVisitante = listaEquiposVisitante[idPartidoRecorrer].Nombre.Split(' ').ToList();

                        foreach (string pal in palabrasApuestaLocal)
                        {
                            foreach (string pel in palabrasEquipoLocal)
                            {
                                if (pal.ToLower() == pel.ToLower())
                                {
                                    listaEquiposLocal[idPartidoRecorrer].PuntosAproximacion =
                                        listaEquiposLocal[idPartidoRecorrer].PuntosAproximacion + pel.Length;
                                    listaEquiposLocal[idPartidoRecorrer].CantidadAproximaciones++;
                                }
                            }
                        }

                        foreach (string pav in palabrasApuestaVisitante)
                        {
                            foreach (string pev in palabrasEquipoVisitante)
                            {
                                if (pav.ToLower() == pev.ToLower())
                                {
                                    listaEquiposVisitante[idPartidoRecorrer].PuntosAproximacion =
                                        listaEquiposVisitante[idPartidoRecorrer].PuntosAproximacion + pev.Length;
                                    listaEquiposVisitante[idPartidoRecorrer].CantidadAproximaciones++;
                                }
                            }
                        }

                        if (
                            listaEquiposLocal[idPartidoRecorrer].PuntosAproximacion > equipoLocalMax.PuntosAproximacion
                            &&
                            listaEquiposLocal[idPartidoRecorrer].CantidadAproximaciones > equipoLocalMax.CantidadAproximaciones
                            )
                        {
                            equipoLocalMax = listaEquiposLocal[idPartidoRecorrer];
                        }

                        if (
                            listaEquiposVisitante[idPartidoRecorrer].PuntosAproximacion > equipoVisitanteMax.PuntosAproximacion
                            &&
                            listaEquiposVisitante[idPartidoRecorrer].CantidadAproximaciones > equipoVisitanteMax.CantidadAproximaciones
                            )
                        {
                            equipoVisitanteMax = listaEquiposVisitante[idPartidoRecorrer];
                        }
                    }

                    if (equipoLocalMax.CantidadAproximaciones > 0 && equipoVisitanteMax.CantidadAproximaciones > 0)
                    {
                        if (equipoLocalMax.Id != equipoVisitanteMax.Id)
                        {
                            if (equipoLocalMax.PuntosAproximacion > equipoVisitanteMax.PuntosAproximacion)
                            {
                                apuestaLocal = listaEquiposLocal[equipoLocalMax.Id - 1].Nombre;
                                apuestaVisitante = listaEquiposVisitante[equipoLocalMax.Id - 1].Nombre;
                            }
                            else
                            {
                                apuestaLocal = listaEquiposLocal[equipoVisitanteMax.Id - 1].Nombre;
                                apuestaVisitante = listaEquiposVisitante[equipoVisitanteMax.Id - 1].Nombre;
                            }
                        }
                        else
                        {
                            apuestaLocal = equipoLocalMax.Nombre;
                            apuestaVisitante = equipoVisitanteMax.Nombre;
                        }

                        if (!EsperarPor(".//div[@class='ipn-TeamStack_Team' and text()='" + apuesta.Local + "']", CLICKEAR: true, INTERVALO: 1, REINTENTOS: 3))
                        {

                            EsperarPor(".//div[@class='ipn-ScoreDisplayBaseLabel_Name' and contains(text(),'" + apuesta.Local + "')]", INTERVALO: 1, CLICKEAR: true, REINTENTOS: 3);
                        }
                        EsperarPor(".//div[@class='ipe-GridHeader_FixtureCell ']");
                        logBet("Partido encontrado");
                        result = true;
                    }
                    else
                    {
                        logBet("No se encontró el partido");
                        apuesta.Basura = true;
                    }
                }
                else
                {
                    logBet("Se ha producido un error al recorrer los equipos");
                }
            }



            return result;
        }

        private string EstandarizarString(string equipo)
        {
            equipo = equipo.ToLower();

            return equipo;
        }

        static int DiferenciaCorners(int cl, int cv)
        {
            return Math.Abs(cl - cv);
        }        

        private void Apostar(Apuesta apuesta)
        {//ok
            try
            {
                logBet("Inicio " + apuesta.Nombre());
                ParametrizacionDeEstrategia(apuesta);
                //setea valores segun estrategia q van a ser usados para apostar las 2 veces
                if (BuscarEncuentro(apuesta))
                {
                    while (!apuesta.Basura) //este loop se repite hasta q se hagan las 2 apuestas
                    {
                        if (VerificacionPrimera(apuesta))
                        {
                            //verificar corners, tiempo, mercado presente y la opcion del mercado deseada. si da true abre el iframe
                            if (EntrarIframeApuesta(apuesta))
                            {
                                //setea el iframe en selenium. de aca en adelante
                                if (VerificacionSegundaIframe(apuesta))
                                {
                                    //verifica cuota dentro del iframe 
                                    if (SetearMontoIFrame(apuesta))
                                    //carga el monto de apuesta en el iframe
                                    {
                                        RealizarApuestaIframe(apuesta);
                                        Task.Run(() => insertarApuesta(apuesta));
                                        //break;  esto no va, porque sino no intenta la seguna apuesta, en lugar del break
                                        //salimos de este loop asi:
                                        if (apuesta.SubApuesta1Finalizada && apuesta.SubApuesta2Finalizada)
                                            apuesta.Basura = true; 
                                    }
                                }
                            }
                        }
                    }
                }

                while (true)
                {
                    try
                    {
                        apuestasPendientes.Remove(apuestasPendientes[0]);
                        break;
                    }
                    catch { }
                }
                Apostando = false;
                logBet("Fin " + apuesta.Nombre());
            }
            catch(Exception ex)
            {
                logBet("La apuesta " + apuesta.Nombre() + " no ha podido ser procesada correctamente. Descripcion: "+ex.Message);
            }            
        }

        private void RealizarApuestaIframe(Apuesta apuesta)
        {
            logBet("Intentando realizar apuesta" + apuesta.Nombre());
            string HTML_BOTON_ACEPTAR_CAMBIOS = ".//a[@class='acceptChanges bs-BtnWrapper']"; //solo para deteccion de presencia
            string HTML_BOTON_ACEPTAR_CAMBIOS_ESCONDIDO = ".//a[@class='acceptChanges bs-BtnWrapper hidden']"; //revisar
            string HTML_BOTON_APOSTAR = ".//a[@class='placeBet bs-BtnWrapper ']"; //revisar
            while (!apuesta.Basura)
            {

                while (true)
                {
                    WebDriver.SwitchTo().DefaultContent();
                    if (EsperarPor(".//div[@class='hm-HeaderModule ']", INTERVALO: 0))
                        break;
                }
                IniciarSesion(USER, PASS);
                while (true)
                {
                    if (apuesta.Current == 1 && apuesta.SubApuesta1Finalizada)
                        break;
                    if (apuesta.Current == 2 && apuesta.SubApuesta2Finalizada)
                        break;
                    EntrarIframeApuesta(apuesta);
                    if (EsperarPor(".//div[@class='bs-Selection_Details']", INTERVALO: 0) || EsperarPor(".//div[@class='br-Selection_Details']", INTERVALO: 0))
                        break;
                }



                if (!EsperarPor(".//div[@class='bs-Selection_Details']", INTERVALO: 0 ) && !EsperarPor(".//div[@class='br-Selection_Details']", INTERVALO: 0)) 
                {
                    logBet("Iframe cerrado, salir");
                    break;
                }
                // verificamos q no este cancelada ninguna de las 2 apuestas por los verificadores anteriores.
                if ((apuesta.Current == 1 && apuesta.SubApuesta1Finalizada) || (apuesta.Current == 2 && apuesta.SubApuesta2Finalizada))
                    break;

                try
                {
                    logBet("Chequeando boton 'Aceptar cambios'");
                    if (EsperarPor(HTML_BOTON_ACEPTAR_CAMBIOS, INTERVALO: 0))
                    {
                        bool aceptarCambios = EsperarPor(".//div[text()='Aceptar cambios']", CLICKEAR: true);
                        if (aceptarCambios)
                        {
                            logBet("Boton 'Aceptar cambios' clickeado");
                        }
                    }

                    //if (EsperarPor(".//span[@class='bs-BtnText' and text()='Apostar']", INTERVALO: 0))
                    logBet("Chequeando boton 'Apostar'");
                    if (EsperarPor(HTML_BOTON_APOSTAR, INTERVALO: 0))
                    {
                        logBet("boton 'Apostar' encontrado");
                        if (!ComprobarCuotaIframe(apuesta)) //si fa false, cancelar apuesta por cuota menor a minima
                            break;
                        WebDriver.FindElementByXPath(".//span[@class='bs-BtnText' and text()='Apostar']").Click(); //si cambia la cuota de bet, se va al catch, poque este boton tendria otro texto
                        logBet("boton 'Apostar' clickeado");
                        //esto espera por los 2 casos, el segundo cancela la espera de primero y continua la ejecucion
                        logBet("esperando por texto 'Apuesta realizada' para finalizar o 'Aceptar cambios' para reintentar");
                        if (!EsperarPor(".//span[@class='br-Header_TitleText' and text() = 'Apuesta realizada']", INTERVALO: 1, REINTENTOS: 10, ALTERNATIVEXPATH: HTML_BOTON_ACEPTAR_CAMBIOS))
                            continue;
                        if (EsperarPor(".//span[@class='br-Header_TitleText' and text() = 'Apuesta realizada']", INTERVALO: 0, REINTENTOS: 10))
                        {
                            apuesta.Resultado = WebDriver.FindElementByXPath(".//span[@class='br-Header_TitleText']").Text;
                            //salir del iframe para sacar la foto
                            while (true)
                            {
                                WebDriver.SwitchTo().DefaultContent();
                                if (EsperarPor(".//div[@class='hm-HeaderModule ']",INTERVALO:0))
                                    break;
                            }
                            imprimirImagen(prefijo:"apuesta");
                            while (true)
                            {
                                EntrarIframeApuesta(apuesta);
                                if (EsperarPor(".//span[@class='br-Header_Done' and text()='Aceptar']",INTERVALO:0))
                                    break;
                            }
                            //regresar al iframe
                            //string "apuesta realizada" cuando aparece podemos salir del loop
                            WebDriver.FindElementByXPath(".//span[@class='br-Header_Done' and text()='Aceptar']").Click();
                            //apuesta realizada
                            logBet("boton 'Aceptar' clickeado");

                            logBet("Apuesta realizada: " + apuesta.Nombre());
                            if (apuesta.Current == 1)
                                apuesta.SubApuesta1Finalizada = true;
                            else if (apuesta.Current == 2)
                                apuesta.SubApuesta2Finalizada = true;
                            logBet("Resultado de apuesta: " + apuesta.Resultado);
                            //apuesta.Insert();
                            break;
                        }
                    }

                    logBet("Checkeando si la apuesta esta suspendida");
                    if (EsperarPor(".//div[text()='Suspendida']", INTERVALO: 0)) //si se suspende la apuesta. nos vamo
                    {
                        logBet("Apuesta suspendida");
                        WebDriver.FindElementByXPath(".//div[text()='Aceptar cambios']").Click();
                        //aceptar cambios y se cierra la apuesta.
                        EsperarPor(".//span[@class='bs-BtnText' and text()='Apostar']", INTERVALO: 1, REINTENTOS: 5);
                    }
                }
                catch
                {
                    if(!EsperarPor(".//div[@class='bs-Selection_Details']", INTERVALO:1,REINTENTOS:3)) //si entra en este if el iframe está cerrado
                    {
                        break;
                    }
                    logBet("Error al intentar realizar apuesta" + apuesta.Nombre());

                }

                logBet("Reintentando apostar y confirmar ");
                Thread.Sleep(500);
            }

            try
            {
                WebDriver.SwitchTo().DefaultContent(); //salimos del iframe de apuesta, muy importante!
                enIframe = false;
            }catch { }

            Thread.Sleep(1000);


            apuesta.Finalizada = true;
        }

        private bool EntrarIframeApuesta(Apuesta apuesta)
        {
            logBet("Intentar entrar en el iframe de apuesta");
            bool result = false;
            IWebElement iframe = null;
            string IFRAME_XPATH = ".//iframe[@class='bw-BetslipWebModule_Frame bw-BetslipWebModule_FrameRHS ']";
            if (EsperarPor(IFRAME_XPATH, INTERVALO: 1, REINTENTOS:5,MOVETO:true))
            {
                iframe = WebDriver.FindElementByXPath(IFRAME_XPATH);
                WebDriver.SwitchTo().Frame(iframe);
                Thread.Sleep(500);
                enIframe = true;
                result = true;
                EsperarPor(".//div[@class='bs-Selection_Details']",ALTERNATIVEXPATH: ".//div[@class='br-Selection_Details']");
                logBet("Iframe abierto");

            }
            else
            {
                logBet("Iframe no abierto, cancelar esta parte de la apuesta");
                if (apuesta.Current == 1)
                    apuesta.SubApuesta1Finalizada = true;
                if (apuesta.Current == 2)
                    apuesta.SubApuesta2Finalizada = true;
            }
            return result;
        }

        private bool VerificacionPrimera(Apuesta apuesta)
        {
            try { WebDriver.SwitchTo().DefaultContent(); } catch { }
            //esta funcion verifica las condiciones para apostar antes de abrir el iframe de apuestas.
            int reintentos = 20;

            while (!apuesta.Basura && reintentos > 0)
            {
                reintentos--;
                IniciarSesion(USER,PASS);

                bool verificarCorners1 = false;
                string CORNERS_XPATH = ".//div[@class='ipe-SoccerGridColumn ipe-SoccerGridColumn_ICorner ']//div";

                #region VerificarCorners1
                logBet("Verificacion corners");
                if (EsperarPor(CORNERS_XPATH, INTERVALO: 1, REINTENTOS: 5))
                {
                    var corners = WebDriver.FindElementsByXPath(CORNERS_XPATH);//scrapeado de bet365
                    apuesta.CornersLocal = int.Parse(corners[1].Text);
                    apuesta.CornersVisitante = int.Parse(corners[2].Text);
                    bool result = apuesta.VerificarCorners();
                    if (!result)
                    {
                        logBet("Se ha marcado un corner. Diferencia con la señal");
                        apuesta.Basura = true;
                        break;                        
                    }else
                        verificarCorners1 = true;
                }
                else
                {
                    logBet("No se pueden leer los corners del partido");
                    verificarCorners1 = false;
                }
                #endregion

                if (verificarCorners1)
                {
                    logBet("Verificar timer");
                    #region VerificarTimer1
                    bool verificarTimer1 = true;
                    string timer = WebDriver.FindElementByXPath(".//div[@class='ipe-SoccerHeaderLayout_ExtraData ']").Text;
                    //string timer2 = WebDriver.FindElementByXPath(".//div[@class='ml1-ScoreHeaderSoccer_Clock ']").Text;
                    var minutos = timer.Split(new string[] { ":" }, StringSplitOptions.None)[0];
                    //var segundos = timer.Split(new string[] { ":" }, StringSplitOptions.None)[1];

                    if (int.Parse(minutos) >= int.Parse(apuesta.MinutosCancelar))
                    {
                        logBet("Cancelar apuesta por estrategia en el tiempo: " + timer);
                        apuesta.Basura = true;
                        verificarTimer1 = false;
                    }

                    if (EsperarPor(".//span[@class='ml1-AnimatedTextBar ml1-CommonAnimations_H2Text ' and text()='Resultado final']", INTERVALO: 0) ||
                        EsperarPor(".//span[@class='ml1-AnimatedTextBar ml1-CommonAnimations_H2Text ' and text()='1ª mitad']", INTERVALO: 0) ||
                        //timer.Contains("45:") || timer.Contains("46:") || timer.Contains("47:") || timer.Contains("48:") || timer.Contains("49:") ||
                        int.Parse(timer.Split(':')[0]) > 44 && apuesta.Estrategia.Equals(Señal.PRIMERAPARTE) 
                        ||
                        //timer.Contains("90:") || timer.Contains("91:") || timer.Contains("92:") || timer.Contains("93:") || timer.Contains("94:")
                        int.Parse(timer.Split(':')[0]) > 89
                        
                        )
                    {
                        logBet("Cancelar apuesta por reloj " + timer);
                        
                        apuesta.Basura = true;
                        verificarTimer1 = false;
                    }

                    #endregion

                    if (verificarTimer1)
                    {
                        logBet("Buscar mercado");
                        #region BuscarMercado
                        bool buscarMercado = false;
                        if (!EsperarPor(".//span[@class='gll-MarketGroupButton_Text' and text()='" + apuesta.MercadoString + "']", REINTENTOS: 10, INTERVALO: 0, MOVETO: true))
                        {
                            logBet("No hay mercado. Reintentando: " + apuesta.MercadoString + " " + apuesta.Nombre());
                            buscarMercado = false;
                            Thread.Sleep(15000);                            
                        }

                        if (EsperarPor(".//span[@class='gll-MarketGroupButton_Text' and text()='" + apuesta.MercadoString + "']", REINTENTOS: 10, INTERVALO: 0))
                        {
                            buscarMercado = true;
                            logBet("Mercado encontrado");
                        }
                        #endregion

                        if (buscarMercado)
                        {
                            #region BuscarOpcionDelMercado
                            bool buscarOpcionDelMercado = false;
                            var caja_apuestas = WebDriver.FindElementByXPath(".//span[@class='gll-MarketGroupButton_Text' and text()='" + apuesta.MercadoString + "']");
                            var parent = caja_apuestas.FindElement(By.XPath("../../div/parent::*"));
                            var botones = parent.FindElements(By.XPath(".//span[@class='gll-ParticipantOddsOnly_Odds']"));
                            var OPCIONES_DE_APUESTA = parent.FindElements(By.XPath(".//span[@class='gll-ParticipantRowValue_Name']"));
                            int indexBotones = 0;

                            logBet("Buscando opcion 'Mas de '" + apuesta.CANTIDAD_CORNERS + " o " + apuesta.CANTIDAD_CORNERS2 + " corners");

                            foreach (var opcion_apuesta in OPCIONES_DE_APUESTA)
                            {
                                if (apuesta.Basura)
                                {
                                    break;
                                }
                                botones = parent.FindElements(By.XPath(".//span[@class='gll-ParticipantOddsOnly_Odds']"));

                                if (opcion_apuesta.Text.Trim().Equals(apuesta.CANTIDAD_CORNERS) && !apuesta.SubApuesta1Finalizada)
                                {
                                    apuesta.Current = 1;
                                    logBet("encontrado 'Mas de " + apuesta.CANTIDAD_CORNERS + "'");
                                    buscarOpcionDelMercado = ClickOpcionApuesta(apuesta, botones[indexBotones]);
                                    break;
                                }
                                else if (opcion_apuesta.Text.Trim().Equals(apuesta.CANTIDAD_CORNERS2) && !apuesta.SubApuesta2Finalizada)
                                {
                                    apuesta.Current = 2;

                                    logBet("encontrado 'Mas de " + apuesta.CANTIDAD_CORNERS2 + "'");
                                    buscarOpcionDelMercado = ClickOpcionApuesta(apuesta, botones[indexBotones]);
                                    break;
                                }
                                else
                                {
                                    try
                                    {
                                        botones[indexBotones].FindElement(By.XPath(".//div[contains(@class,'Suspended')]"));
                                        logBet("Mercado suspendido");
                                    }
                                    catch { }

                                }
                                indexBotones = indexBotones + 1;
                            }
                            #endregion

                            if (buscarOpcionDelMercado)
                                return true;
                        }
                    }                        
                }                
            }
           
            return false;
        }

        private bool VerificacionSegundaIframe(Apuesta apuesta)
        {

            bool result = false;
            if (EsperarPor(".//div[@class='bs-Selection_Details']", INTERVALO: 0) || EsperarPor(".//div[@class='br-Selection_Details']", INTERVALO: 0))
            {
                if (ComprobarCuotaIframe(apuesta))
                {
                    result = true;
                }
            }
            else
            {
                logBet("iframe no abierto");
            }

            return result;
        }

        private void ParametrizacionDeEstrategia(Apuesta apuesta)
        {
            apuesta.SubApuesta1Finalizada = false;
            apuesta.SubApuesta2Finalizada = false;
            //Apuesta apuesta2 = apuesta.Clonar();
            switch (apuesta.Estrategia)
            {
                case Señal.CORNERSHT:
                    //estrategia 1
                    apuesta.MercadoString = "1ª mitad - Córners asiáticos";
                    int difcor = DiferenciaCorners(apuesta.CornersLocal, apuesta.CornersVisitante);
                    apuesta.MinutosCancelar = "39";
                    apuesta.MinutosCancelar2 = "42";
                    switch (difcor)
                    {
                        case 0:
                            apuesta.CANTIDAD_CORNERS = "1";
                            apuesta.CANTIDAD_CORNERS2 = "0.5";

                            break;
                        case 1:
                            apuesta.CANTIDAD_CORNERS = "2";
                            apuesta.CANTIDAD_CORNERS2 = "1.5";
                            break;

                        default:
                            logBet("Apuesta cancelada " + apuesta.Nombre());
                            logBet("Diferencia de corners: " + difcor);
                            logBet("Corners local: " + apuesta.CornersLocal);
                            logBet("Corners visitante: " + apuesta.CornersVisitante);
                            break;
                    }
                    break;

                case Señal.PRIMERAPARTE:
                    //estrategia 2
                    apuesta.MercadoString = "1ª mitad - Córners asiáticos";
                    apuesta.MinutosCancelar = "39";
                    apuesta.MinutosCancelar2 = "42";
                    apuesta.CANTIDAD_CORNERS = (apuesta.CornersLocal + apuesta.CornersVisitante + 1).ToString().Replace(",", ".");
                    apuesta.CANTIDAD_CORNERS2 = (apuesta.CornersLocal + apuesta.CornersVisitante + 0.5).ToString().Replace(",", ".");
                    break;

                case Señal.SEGUNDAPARTE:
                    //estrategia 3
                    apuesta.MercadoString = "Córners asiáticos";
                    apuesta.MinutosCancelar = "89";
                    apuesta.MinutosCancelar2 = "99";//esto desactiva la validacion, para q espere el final del partido nada mas.
                    apuesta.CANTIDAD_CORNERS = (apuesta.CornersLocal + apuesta.CornersVisitante + 1).ToString().Replace(",", "."); ;
                    apuesta.CANTIDAD_CORNERS2 = (apuesta.CornersLocal + apuesta.CornersVisitante + 0.5).ToString().Replace(",", "."); ;
                    break;
            }
            logBet("Apuesta parametrizada");
        }


        private bool ClickOpcionApuesta(Apuesta apuesta, IWebElement boton)
        {
            bool result = false;
            if (double.Parse(boton.Text) >= apuesta.CuotaMinima)
            {
                boton.Click();
                //este click abre el iframe para apostar
                result = EsperarPor(".//iframe[@class='bw-BetslipWebModule_Frame bw-BetslipWebModule_FrameRHS ']"); //acaca
                if (EsperarPor(".//iframe[@class='bw-BetslipWebModule_Frame bw-BetslipWebModule_FrameRHS ']", INTERVALO: 0))
                    logBet("iframe abierto con exito");
            }
            else
            {
                if (apuesta.Current == 1)
                    apuesta.SubApuesta1Finalizada = true;
                else if (apuesta.Current == 2)
                    apuesta.SubApuesta2Finalizada = true;
                logBet("Cuota muy baja");
            }
            return result;
        }

        private bool SetearMontoIFrame(Apuesta apuesta)
        {
            bool result = false;
            if (apuesta.Current == 2)
                apuesta.Monto *= 2;
            logBet("Intentando setear monto: " + apuesta.Monto);

            while (true)
            {
                var campo_monto_bet = WebDriver.FindElementByXPath(".//input[@class='stk bs-Stake_TextBox' and @placeholder='Imp.']").GetAttribute("value");
                var monto_para_campo = apuesta.Monto.ToString().Replace(".", ",");

                if (campo_monto_bet.Contains(monto_para_campo))
                { //si el campo para el monto de bet365  tiene el monto que queremos entonces salimos de este loop.
                    result = true;
                    break;
                }

                try
                {
                    //loop hasta colocar el monto deceado en el campo para monto de apuesta de bet365
                    EsperarPor(".//input[@class='stk bs-Stake_TextBox']");
                    WebDriver.FindElementByXPath(".//input[@class='stk bs-Stake_TextBox' and @placeholder='Imp.']").Click();
                    Thread.Sleep(200);
                    WebDriver.FindElementByXPath(".//input[@class='stk bs-Stake_TextBox' and @placeholder='Imp.']").Clear();
                    Thread.Sleep(200);
                    WebDriver.FindElementByXPath(".//input[@class='stk bs-Stake_TextBox' and @placeholder='Imp.']").SendKeys(apuesta.Monto.ToString());
                    Thread.Sleep(200);

                }
                catch
                { }
            }

            return result;
        }

        private bool ComprobarCuotaIframe(Apuesta apuesta)
        {
            bool result = true;
            try
            {
                EsperarPor(".//div[@class='bs-Odds']", REINTENTOS: 5, INTERVALO: 1);
                double cuota_actual = double.Parse(WebDriver.FindElementByXPath(".//div[@class='bs-Odds']").Text);//obtener cuota de bet
                if (cuota_actual < apuesta.CuotaMinima * 1000)
                {
                    logBet("Cancelar apuesta, la cuota se ha modificado: " + cuota_actual);
                    imprimirImagen(prefijo: "CANCELAR_ComprobarCuotaIframe");
                    apuesta.Resultado = "Cancelada";
                    result = false;
                    //cancelamos apuesta por cuota minima
                    if (apuesta.Current == 1)
                        apuesta.SubApuesta1Finalizada = true;
                    else if (apuesta.Current == 2)
                        apuesta.SubApuesta2Finalizada = true;
                }
                else
                {
                    logBet("Cuota aceptable: " + cuota_actual);
                }

            }
            catch
            {
                logBet("Error al intentar leer cuota");
                imprimirImagen(prefijo: "ERROR_ComprobarCuotaIframe");
                if (apuesta.Current == 1)
                    apuesta.SubApuesta1Finalizada = true;
                else if (apuesta.Current == 2)
                    apuesta.SubApuesta2Finalizada = true;
            }            
            return result;
        }
    }
}

