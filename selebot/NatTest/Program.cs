﻿using Bet365;
using Modelo.NATS;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace NatTest
{
    class Program
    {
         
       static NatsHandler natsHandler = new NatsHandler("nats://ec2-3-124-242-27.eu-central-1.compute.amazonaws.com");

        static void Main(string[] args)
        {

            //ejemplo de señal.txt

            //ALERTA[Primera parte]

            //Paraguay Cup / SV Dakota v Nacional

            //Placar: 3 - 0
            //Tempo: 81' (2º Tempo)

            //Chutes ao gol: 5 - 4

            //Chutes fora do gol: 4 - 6

            //Cantos: 4 - 10

            //Posse de bola: 50 - 50
            //Cartões vermelhos: 0 - 0


            //PI1: 20 - 30
            //PI2: 12 - 12

            string mensajeRaw = File.ReadAllText(@"señal.txt", Encoding.UTF8);


            var resultString = Regex.Replace(mensajeRaw, @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline);
            string[] lineas = resultString.Split(new[] { Environment.NewLine }, StringSplitOptions.None);


            var titulo = lineas[0];
            var liga_equipos = lineas[1];
            var liga = liga_equipos.Split('/')[0];

            var vEquiposAmbos = liga_equipos.Split('/')[1];
            var equipo_local = vEquiposAmbos.Split(new string[] { " v " }, StringSplitOptions.None)[0].Trim();
            var equipo_visitante = vEquiposAmbos.Split(new string[] { " v " }, StringSplitOptions.None)[1].Trim();

            var marcador_local = lineas[2].Replace("Placar:", "").Split('-')[0].Trim();
            var marcador_visitante = lineas[2].Replace("Placar:", "").Split('-')[1].Trim();
            var tiempo_texto = lineas[3];
            var tiros_arco_local = lineas[4].Replace("Chutes ao gol:", "").Split('-')[0].Trim();
            var tiros_arco_visitante = lineas[4].Replace("Chutes ao gol:", "").Split('-')[1].Trim();
            var tiros_fuera_local = lineas[5].Replace("Chutes fora do gol:", "").Split('-')[0].Trim();
            var tiros_fuera_visitante = lineas[5].Replace("Chutes fora do gol:", "").Split('-')[1].Trim();
            var corners_local = lineas[6].Replace("Cantos:", "").Split('-')[0].Trim();
            var corners_visitante = lineas[6].Replace("Cantos:", "").Split('-')[1].Trim();
            var posesion_balon_local = lineas[7].Replace("Posse de bola:", "").Split('-')[0].Trim();
            var posesion_balon_visitante = lineas[7].Replace("Posse de bola:", "").Split('-')[1].Trim();
            var tarjetas_rojas_local = lineas[8].Replace("Cartões vermelhos:", "").Split('-')[0].Trim();
            var tarjetas_rojas_visitante = lineas[8].Replace("Cartões vermelhos:", "").Split('-')[1].Trim();
            var PI1_local = lineas[9].Replace("PI1:", "").Split('-')[0].Trim();
            var PI1_visitante = lineas[9].Replace("PI1:", "").Split('-')[1].Trim();
            var PI2_local = lineas[10].Replace("PI2:", "").Split('-')[0].Trim();
            var PI2_visitante = lineas[10].Replace("PI2:", "").Split('-')[1].Trim();



            Señal señal = new Señal();
            señal.MensajeRAW = mensajeRaw;
            señal.MarcadorLocal = marcador_local;
            señal.EquipoVisitante = marcador_visitante;
            señal.EquipoLocal = equipo_local;
            señal.EquipoVisitante = equipo_visitante;
            señal.CornersLocal = corners_local;
            señal.CornersVisitante = corners_visitante;
            señal.PI1Local = PI2_local;
            señal.PI1Visitante = PI2_visitante;




            señal.SetEstrategia(titulo);

            natsHandler.Send(señal);
            Thread.Sleep(1000);
            Environment.Exit(1);
        }
    }
}
