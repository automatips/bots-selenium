﻿using System;

namespace Bet365
{
    public class Señal
    {


        public string MensajeRAW { get; set; }
        public string MarcadorLocal { get; set; }
        public string EquipoVisitante { get; set; }
        public string EquipoLocal { get; set; }
        public string CornersLocal { get; set; }
        public string CornersVisitante { get; set; }
        public string PI1Local { get; set; }
        public string PI1Visitante { get; set; }
        public string Estrategia { get; set; }



        public const string CORNERSHT = "CORNERSHT";
        public const string SEGUNDAPARTE = "SEGUNDAPARTE";
        public const string PRIMERAPARTE = "PRIMERAPARTE";

        public void SetEstrategia(string titulo)
        {
            if (titulo.Replace(" ", "").Equals("ALERTA[CornersHT]"))
            {
                Estrategia = CORNERSHT;
            }
            else if (titulo.Replace(" ", "").Equals("ALERTA[Segundaparte]"))
            {
                Estrategia = SEGUNDAPARTE;

            }
            else if (titulo.Replace(" ", "").Equals("ALERTA[Primeraparte]"))
            {
                Estrategia = PRIMERAPARTE;
            }
            else
            {
                //no machea ninguna estrategia..
                Estrategia = null;

            }
        }
    }
}