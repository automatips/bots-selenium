﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Bet365
{
    public partial class Pais : ISUD<Pais>
    {
        public Pais() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdPais { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Nombre { get; set; }

        public string FHBaja { get; set; }
    }
}