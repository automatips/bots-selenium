﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Bet365
{
    public partial class Apuesta : ISUD<Apuesta>
    {
        public Apuesta() : base(1) { } // base(SPs_Version)

        // Properties

        public long IdApuesta { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Local { get; set; }

        public string Visitante { get; set; }

        public string Equipo { get; set; }

        public string EquipoContrario { get; set; }

        public decimal? Monto { get; set; }
        public double CuotaMinima { get; set; }

        public int? IdCuenta { get; set; }

        public string Resultado { get; set; }
    }
}