﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace TelgramBot
{
    public partial class SeñalInplayScanner : ISUD<SeñalInplayScanner>
    {
        public SeñalInplayScanner() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdSeñalInplayScanner { get; set; }

        public DateTime? FHAlta { get; set; }

        public string titulo { get; set; }

        public string liga_equipos { get; set; }

        public string liga { get; set; }

        public string vEquiposAmbos { get; set; }

        public string equipo_local { get; set; }

        public string equipo_visitante { get; set; }

        public string marcador_local { get; set; }

        public string marcador_visitante { get; set; }

        public string tiempo_texto { get; set; }

        public string tiros_arco_local { get; set; }

        public string tiros_arco_visitante { get; set; }

        public string tiros_fuera_local { get; set; }

        public string tiros_fuera_visitante { get; set; }

        public string corners_local { get; set; }

        public string corners_visitante { get; set; }

        public string posesion_balon_local { get; set; }

        public string posesion_balon_visitante { get; set; }

        public string tarjetas_rojas_local { get; set; }

        public string tarjetas_rojas_visitante { get; set; }

        public string PI1_local { get; set; }

        public string PI1_visitante { get; set; }

        public string PI2_local { get; set; }

        public string PI2_visitante { get; set; }
    }
}