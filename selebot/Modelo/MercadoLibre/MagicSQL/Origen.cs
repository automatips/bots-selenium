﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace MercadoLibre
{
    public partial class Origen : ISUD<Origen>
    {
        public Origen() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdOrigen { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Nombre { get; set; }

        public DateTime? FHUltimoUso { get; set; }
    }
}