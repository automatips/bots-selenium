﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace DataFirst
{
    public partial class Error : ISUD<Error>
    {
        public Error() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdError { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Descripcion { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

        public string Sistema { get; set; }

        public void Ingresar(string Descripcion, Exception ex,string Sistema)
        {
            try
            {
                Error er = new Error();
                er.Descripcion = Descripcion;
                er.Sistema = Sistema;

                if (ex != null)
                {
                    er.Message = ex.Message;
                    er.StackTrace = ex.StackTrace;
                }
                er.FHAlta = DateTime.Now;
                er.Insert();
            }
            catch { }            
        }
    }
}