﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Facebook
{
    public partial class Post : ISUD<Post>
    {
        public Post() : base(1) { } // base(SPs_Version)

        // Properties

        public long IdPost { get; set; }

        public DateTime? FHAlta { get; set; }

        public string GrupoNombre { get; set; }

        public string GrupoLink { get; set; }

        public string PerfilNombre { get; set; }

        public string PerfilLink { get; set; }

        public string Contenido { get; set; }

        public string Permalink { get; set; }

        public string ImgSrc { get; set; }

        public string ImgAlt { get; set; }

        public string PerfilFoto { get; set; }
    }
}