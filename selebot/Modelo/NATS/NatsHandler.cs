﻿using NATS.Client;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bet365;


namespace Modelo.NATS
    
{
    public class NatsHandler
    {
        private static string TKN = "a9s7ca!ASKL$DIOSA$KJed7?d8SDfSD934dfas@";

        public static IConnection Conection;

        private static string Subject { get; set; }
        public static char[] Payload { get; set; }
        static public EventHandler<MsgHandlerEventArgs> FuncionEvent { get; private set; }
        public static Options opts { get; set; }

        public NatsHandler(string ipserver)
        {
            opts = ConnectionFactory.GetDefaultOptions();
            opts.Url = ipserver;
            opts.AllowReconnect = true;
            opts.ReconnectWait = 500;
            opts.MaxReconnect = Options.ReconnectForever;
            opts.Token = TKN;
            Conection = new ConnectionFactory().CreateConnection(opts);
            Subject = "main.debug";
        }

        public bool Running()
        {
            if (Conection.IsClosed())
            {
                Conection = new ConnectionFactory().CreateConnection(opts);
            }
            if (Conection.IsReconnecting())
            {
                while (true) { if (!Conection.IsReconnecting()) { break; } }

            }

            return !(Conection.IsClosed() || Conection.IsReconnecting());
        }

        static async Task SendAsync()
        {
            if (Conection.IsClosed())
            {
                Conection = new ConnectionFactory().CreateConnection(opts);
            }
            if (Conection.IsReconnecting())
            {
                while (true) { if (!Conection.IsReconnecting()) { break; }}

            }

            Conection.Publish(Subject, Encoding.UTF8.GetBytes(Payload));
        }

        internal static async Task SuscribeAsync()
        {
            if (Conection.IsClosed())
            {
                Conection = new ConnectionFactory().CreateConnection(opts);
            }

            if (Conection.IsReconnecting())
            {
                while (true)
                    if (!Conection.IsReconnecting())
                        break;
            }

            IAsyncSubscription sAsync = Conection.SubscribeAsync(Subject);
            sAsync.MessageHandler += FuncionEvent;
            sAsync.Start();

            //using (Conection)
            //{
            //    using (ISyncSubscription s = Conection.SubscribeSync(Subject))
            //    {
            //       Msg m = s.NextMessage();
            //       System.Console.WriteLine("Received: " + m);

            //    }
            //}
        }

        public void Suscribe(EventHandler<MsgHandlerEventArgs> eh)
        {
            FuncionEvent = eh;
            Task task = Task.Run(SuscribeAsync);
        }
        public void Send(Object señal)
        {
            string json = JsonConvert.SerializeObject(señal, Formatting.Indented);
            Payload = json.ToCharArray();
            Task task = Task.Run(SendAsync);
        }


        static EventHandler<MsgHandlerEventArgs> h = (sender, args) =>
        {
            //Console.WriteLine(args.Message);
            string data = Encoding.UTF8.GetString(args.Message.Data, 0, args.Message.Data.Length);
            Señal señal = JsonConvert.DeserializeObject<Señal>(data);
        };
    }
}
