﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Workana
{
    public partial class ProyectoFreelancer : ISUD<ProyectoFreelancer>
    {
        public ProyectoFreelancer() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdProyectoFreelancer { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Url { get; set; }

        public int? Puntos { get; set; }

        public byte? MarcadoFavorito { get; set; }

        public byte? Postulado { get; set; }
    }
}