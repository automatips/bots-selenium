﻿// Created for MagicSQL using MagicMaker [v.3.75.101.7049]

using System;
using MagicSQL;

namespace Workana
{
    public partial class Usuario : ISUD<Usuario>
    {
        public Usuario() : base(1) { } // base(SPs_Version)

        // Properties

        public int IdUsuario { get; set; }

        public DateTime? FHAlta { get; set; }

        public string Nombre { get; set; }

        public int? Calificacion { get; set; }

        public string MiembroDesde { get; set; }

        public string Pais { get; set; }

        public bool? PagoVerificado { get; set; }
    }
}