﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="WorkanaBotWeb.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="container-fluid">
			<asp:UpdatePanel ID="upGridViewProyectos" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="false" style="margin-left: 10px; margin-right: 10px;">
		        <ContentTemplate>
                    <asp:GridView ID="gvProyectos" CssClass="table table-striped table-bordered table-hover grid-view"
                        Width="100%" AutoGenerateColumns="False" runat="server"
                        EmptyDataText="Sin datos para mostrar."
                        OnSelectedIndexChanging="gvProyectos_SelectedIndexChanging" OnRowDeleting="gvProyectos_RowDeleting"
                        >
                        <Columns>
                           <asp:BoundField HeaderText="#" DataField="IdProyecto" HeaderStyle-Width="25" ItemStyle-Width="25"/>

                            <asp:TemplateField HeaderText="" ItemStyle-Width="1">
                                <ItemTemplate>
                                    <center>
                                        <asp:LinkButton 
                                            ID="btnPostular"
                                            runat="server"
                                            CssClass="btn btn-success"
                                            Style="margin-left: 5px;"
                                            title="Marcar como postulado"
                                            CommandName="Select"
                                            >
                                            <span aria-hidden="true" class="glyphicon glyphicon-plus"></span>
                                        </asp:LinkButton>

                                        <asp:LinkButton 
                                            ID="btnEliminar"
                                            runat="server"
                                            CssClass="btn btn-danger"
                                            Style="margin-left: 5px;"
                                            title="Marcar como descartado"
                                            CommandName="Delete"
                                            >
                                            <span aria-hidden="true" class="glyphicon glyphicon-check"></span>
                                        </asp:LinkButton>
                                    </center>
                                </ItemTemplate>
                            </asp:TemplateField>

                           <asp:HyperLinkField
								DataNavigateUrlFields="Url"
								DataNavigateUrlFormatString="{0}"
                                Target="_blank"
								DataTextField="Titulo"
								HeaderText="Titulo"
								SortExpression="Titulo"
                                ItemStyle-Width="100"
                               >
						   </asp:HyperLinkField>

                           <asp:HyperLinkField
                                Target="_blank"
								DataTextField="Detalles"
								HeaderText="Detalles"
								SortExpression="Detalles"
                                HeaderStyle-Width="200"
                                ItemStyle-Width="200"
                               >                               
						   </asp:HyperLinkField>

                          <asp:BoundField HeaderText="Fecha de Alta" DataField="FHAlta" ItemStyle-CssClass="date-time" HeaderStyle-Width="50" ItemStyle-Width="50"/>
                            
                           <asp:HyperLinkField
                                Target="_blank"
								DataTextField="Presupuesto"
								HeaderText="Presupuesto"
								SortExpression="Presupuesto"
                                HeaderStyle-Width="50"
                                ItemStyle-Width="50"
                               >                               
						   </asp:HyperLinkField>

                           <asp:HyperLinkField
                                Target="_blank"
								DataTextField="Pais"
								HeaderText="Pais"
								SortExpression="Pais"
                                HeaderStyle-Width="50"
                                ItemStyle-Width="50"
                               >                               
						   </asp:HyperLinkField>

                           <asp:HyperLinkField
                                Target="_blank"
								DataTextField="Puntos"
								HeaderText="Puntos"
								SortExpression="Puntos"
                                HeaderStyle-Width="25"
                                ItemStyle-Width="25"
                               >                               
						   </asp:HyperLinkField>

                           <asp:HyperLinkField
                                Target="_blank"
								DataTextField="UltimoEstado"
								HeaderText="UltimoEstado"
								SortExpression="UltimoEstado"
                                HeaderStyle-Width="50"
                                ItemStyle-Width="50"
                               >                               
						   </asp:HyperLinkField>

                        </Columns>
                    </asp:GridView>
		        </ContentTemplate>
	        </asp:UpdatePanel>
        </div>
    <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick">
    </asp:Timer>
</asp:Content>
