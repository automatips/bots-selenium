﻿using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MagicSQL;
using Bet365;

namespace DataFirst
{   //prueba logi 2
    //public int Id;
    //public IWebDriver WebDriver;
    public class Bot
    {
        public FirefoxDriver WebDriver { get; private set; }
        public int Id { get; set; }
        public string UrlActual { get; set; }
        public Bet365.Proxy proxyBet365 { get ; set; }
        public void InicializarWebdriver()
        {
            if (WebDriver == null)
            {
                FirefoxOptions opciones = new FirefoxOptions();
                FirefoxProfileManager profileManager = new FirefoxProfileManager();
                FirefoxProfile profile = profileManager.GetProfile("JuanBot");

                var a = profileManager.ExistingProfiles;

                if (proxyBet365 != null)
                {
                    OpenQA.Selenium.Proxy proxy = new OpenQA.Selenium.Proxy();
                    proxy.HttpProxy = proxyBet365.IP + ":" + proxyBet365.Puerto;
                    proxy.FtpProxy = proxyBet365.IP + ":" + proxyBet365.Puerto;
                    proxy.SslProxy = proxyBet365.IP + ":" + proxyBet365.Puerto;
                    opciones.Proxy = proxy;
                }                

                //opciones.AddAdditionalCapability("marionette", "marionette");
                WebDriver = new FirefoxDriver(opciones);
                WebDriver.Manage().Timeouts().PageLoad = TimeSpan.FromSeconds(40);
                Console.Clear();
            }
        }

        public void IrA(String url)
        {
            try
            {
                WebDriver.Navigate().GoToUrl(url);
            }
            catch (Exception e) { Console.WriteLine(e); }
        }

        public string InfoIp()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://ipinfo.io/?token=725314390c5b44");
            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            JObject json;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                var a = reader.ReadToEnd();
                json = JObject.Parse(a);
            }
            var e = json["country"].ToString();
            var vTimeZone = json["timezone"].ToString();
            string[] vTimeZoneSplit = vTimeZone.Split('/');
            string vPais = vTimeZoneSplit[1];
            //ip  "181.165.194.225"
            //hostname    "225-194-165-181.fibertel.com.ar"
            //city    "Córdoba"
            //region  "Cordoba"
            //country "AR"
            //loc "-31.4135,-64.1810"
            //org "AS10318 Telecom Argentina S.A."
            //postal  "5000"
            //timezone    "America/Argentina/Cordoba"

            Console.WriteLine("navegando desde: " + json["city"] + " " + json["country"]);
            Console.WriteLine("con la ip '" + json["ip"] + "' del pais " + vPais);
            return e;
        }
    }
}
